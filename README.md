# Xavier Lab Documentation
Repository for hosting documentation related to pipelines, analysis, etc. We are very much in the process of building our documentation, so please feel free to offer suggestions for how you would like to see it done!

The master version of the documentation is located [here](https://xavier-lab.readthedocs.io/en/master/?) and the dev version can be found [here](https://xavier-lab.readthedocs.io/en/dev/?). Both versions are set up to automatically reflect updates in their associated branches (master and dev, respectively).

If you would like to create a new branch and see how your changes look on a proper webpage, get in touch with Christian and he will add your branch to those being tracked.
