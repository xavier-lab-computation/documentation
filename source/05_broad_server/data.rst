.. _data:

Data
****

Host
====
Host-side data that is not immediately placed on Google Cloud usually ends up being placed on the Broad server at */broad/xavierlab_datadeposit/*.
Users should be able to create their own directories here and use it as storage as they work with data. If you are unable to create a directory, it is likely
because you have not yet been added as a member of the *xavierlab* group. You should contact the host-side project manager for help with this. 

Microbiome
==========
Work in progress...
