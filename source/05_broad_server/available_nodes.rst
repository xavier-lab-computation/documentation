.. _available_nodes:

Available Nodes
***************

Login Nodes
===========
You can access login nodes called *silver*, *gold*, and *platinum* which you can call by name.
You can also simply use the name *login*, which will direct you to a numbered login node.
These nodes should not be used for any serious computation or large file transfers,
but they can be used to view the file system or start interactive sessions as
described `here <https://intranet.broadinstitute.org/node/5205/>`_.

Lab-Owned Nodes
===============
The lab has access to two nodes, *boltzmann* and *flash-c001*. These have more resources (in terms of both cores and RAM) than do the login nodes, and thus are useful for
carrying out large file transfers or doing other moderate-to-large computational tasks. 
