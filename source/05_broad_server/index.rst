.. _05_broad_server:

Broad Server
************

General information regarding computing on the Broad Server.

.. toctree::
    :maxdepth: 1
    :titlesonly:

    general_tips
    available_nodes
    software
    data
