.. _general_tips:

General Tips
************

Using the Broad Server
======================
You will be able to ssh into the Broad Server using *ssh <x>*, were *x* is any of the available nodes, see :ref:`available_nodes`. In order to do this, you must either be directly
on the Broad network or be using the VPN as described `here <https://intranet.broadinstitute.org/node/5188>`_.  

Broad IT Support
================
Documentation written by Broad Information Technology Services (BITS) can be found `here <https://intranet.broadinstitute.org/node/5063>`_.
