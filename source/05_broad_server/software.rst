.. _software:

Software
********

Dotkits
========
Using software on the Broad server is done via `dotkits <https://intranet.broadinstitute.org/node/5124>`_.
To see a list of all the pre-installed dotkits on the server, use the command *use -la*. If you want to search for dotkits containing
a particular search term, you can use *usa -la | grep <search_term>*. Once you have decided on a piece of software you would
like to run, entering *use <software_name>* into the command line will load the software into your local environment.

Shell Configuration
===================
In order to configure your shell on the Broad server, you can edit the file located at *~/.my.bashrc*.
This file will be run as a shell script each time you open a shell on the Broad server, so it is a convenient way to load oft-used software or set shortcuts to commonly
accessed file locations.
Do not edit the regular *~/.bashrc* file as it is system-supplied.
There is a simple example located `here <https://gitlab.com/xavier-lab-computation/utilities/configuration/sample.my.bashrc>`_. At the very least, it is generally seen
as a good idea to add some of the basic dotkits (e.g. UGER and the Google Cloud SDK) to your bashrc.
