.. _acknowledgements:

Acknowledgements and Getting Help
*********************************
We would like to thank the following parties for providing software, ideas, and support in building our pipelines. This is not only to give credit to the
many people and organizations whose work makes what we do possible, but also to help users identify relevant contacts for various pipelines.

* General Software
    * General Purpose and Statistical Programming:
        * `python <https://www.python.org/>`_
        * `R <https://www.r-project.org/>`_
    * Documentation:
        * `reStructuredText <http://docutils.sourceforge.net/rst.html>`_: language used for documentation source files
        * `Sphinx <http://www.sphinx-doc.org/en/master/>`_: documentation creation tool
    * Pipeline Tools:
        * `Cromwell <https://cromwell.readthedocs.io/en/stable/>`_: workflow management system
        * `WDL <https://software.broadinstitute.org/wdl/>`_: language used for Cromwell workflows
* Pipeline-Specific Software
    * Host
        * :ref:`crispr_screen`
            * Count generation
                * `poolq <https://portals.broadinstitute.org/gpp/public/software/poolq>`_: generates counts from pooled screen
            * Differential Expression
                * `edgeR <https://bioconductor.org/packages/release/bioc/html/edgeR.html>`_: standard differential expression tool
            * Genetic Perturbation Ranking
                * `STARS <https://portals.broadinstitute.org/gpp/public/software/stars>`_: gene-ranking algorithm for genetic perturbation screens
        * :ref:`rna_seq`
            * Alignment/Counting
                * `kallisto <https://pachterlab.github.io/kallisto/>`_: pseudoalignment and transcript counts
            * Demultiplexing
                * `bcl2fastq <https://support.illumina.com/downloads/bcl2fastq-conversion-software-v2-20.html>`_: convert from bcl files to fastq
            * Differential Gene Expression analysis
                * `sleuth <https://pachterlab.github.io/sleuth/>`_: differential expression companion to kallisto
            * QC Analysis
                * `FastQC <https://www.bioinformatics.babraham.ac.uk/projects/fastqc/>`_: pre-alignment QC
                * `MultiQC <https://multiqc.info/>`_: combines QC from multiple sources and creates HTML
    * Microbiome
* General Support
    * Christian Covington
        * Primary point person for host-side infrastructure
    * Josh Gould
        * Provided insight into general pipeline creation and running pipelines on Firecloud
        * Wrote command-line tool for submitting Firecloud workflows
    * Damian Plichta
        * Primary point person for microbiome-side infrastructure
    * Mukund Varma
        * Author of original version of on-prem RNA-Seq pipeline
