.. _contributing:

Contributing
************

We encourage users to contribute in any way they see fit. Ways to contribute (and steps associated with each) are listed below:

Adding or editing a pipeline
============================
1. Add the new/edited WDL script to the Firecloud method repository and associate it with the appropriate workspace.

2. Add the new/edited WDL script, a sample configuration file, and a Dockerfile (or directory including Dockerfile and other scripts to be included in the container) to the
`pipelines repository <https://gitlab.com/xavier-lab-computation/pipelines>`_.

3. Add/augment pipeline documentation to :ref:`03_pipelines`. If possible, please try to maintain the same documentation structure as
existing pipelines.

.. note::
    Our documentation currently assumes that all pipelines use WDL+Cromwell in FireCloud, but this could be amended if desired.

Adding utilities
================
1. Add the script to the `utilities repository <https://gitlab.com/xavier-lab-computation/utilities>`_.
2. Add documentation to the appropriate *utilities* subsection.

Contributing to documentation
=============================
Our documentation is created with the Sphinx document generation tool and is written the reStructuredText markup language.
A primer on reStructuredText (specifically with Sphinx in mind) can be found `here <http://www.sphinx-doc.org/en/master/usage/restructuredtext/>`_.

General Structure
-----------------
The `source files <https://gitlab.com/xavier-lab-computation/documentation/tree/master/source>`_ for the documentation follow a particular structure that we want to keep consistent.
Each subdirectory in the source files (representing different sections of documentation) has a name beginning with a number (e.g. *01_intro*). The goal here is to maintain consistency between the order of documentation
on the website and the order of the subdirectories. If you want to add a new subdirectory, please maintain this naming convention. If you want your newly written section of the documentation to be the third section (for example),
then use *03_* as the prefix to your subdirectory name. You will need to renumber all the subdirectories that appear after your insertion.

The *index.rst* files enforce the organization and ordering of the other files. There is a master index file that calls the index file
of each subdirectory, which in turn calls the component *.rst* files containing actual content. When you add a new subdirectory or a new individual
section to an existing subdirectory, you will need to add that file to the corresponding index.

Tips
----
We also recommend that you keep consistent the label at the top of the file (of the form '.. _<label_here>') and the section header (the
text above the line of asterisks). This will hopefully make it easier to cross-reference documents as seen
`here <http://www.sphinx-doc.org/en/stable/usage/restructuredtext/roles.html#cross-referencing-arbitrary-locations>`_.

We try to be consistent with our use of section titles, the possibilities of which are enumerated
`here <http://docutils.sourceforge.net/docs/ref/rst/restructuredtext.html#sections>`_. The top three levels we use are \*, =, and -.
Feel free to add more levels if you need to create a documentation page that contains more than three levels of organization.

If you have further questions, please get in touch with Christian Covington.

Comments/Concerns
=================
If you have any other thoughts (bugfixes, feature requests, etc.), please open a `Gitlab issue <https://gitlab.com/xavier-lab-computation>`_.
