.. _06_misc:

Miscellaneous
*************

.. toctree::
    :maxdepth: 1
    :titlesonly:

    contributing
    acknowledgements
