.. _02_setup:

Setup
*****

.. toctree::
    :maxdepth: 1
    :titlesonly:

   google_cloud_setup
   terra_setup
