.. _03_pipelines:

Pipelines
*********

Location of pipeline overviews, configuration information, and other good to know information.

.. toctree::
    :caption: Usage:
    :maxdepth: 1
    :titlesonly:

    usage/index

.. toctree::
    :caption: Host:
    :maxdepth: 1
    :titlesonly:

    3-5_prime_rna_seq/index
    bcl2fastq/index
    bulk_rna_seq/index
    simple_variant_calling/index
    stars_crispr_screen/index
    tcr_seq/index
    tf_seq/index

.. toctree::
    :caption: Microbiome:
    :maxdepth: 1
    :titlesonly:
