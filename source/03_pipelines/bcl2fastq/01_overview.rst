.. _bcl2fastq_overview:

Overview
********

FireCloud Method: xavier-genomics/bcl2fastq

This 'pipeline' is much smaller in scope than a standard pipeline. It simply take *bcl* files, converts them into *fastq* files, and generates a
*read_pairs.tsv* file describing the read pairs.

This is equivalent to the first step of the RNA-Seq pipeline, so the only reason to use this one is if you need to do *bcl2fastq* outside of the normal
RNA-Seq protocol. For example, if you have already run the RNA-Seq pipeline on one set of data and now you are being asked to combine those data with
another set. You may want to use this pipeline to convert the new data to *fastq*, at which point you can combine the runs and run them all through
the RNA-Seq pipeline again.

Note that the `bcl2fastq` parameters used inside this pipeline were chosen to be consistent with what we normally do for paired-end bulk RNA-Seq.
They may not be appropriate for other types of sequencing.
