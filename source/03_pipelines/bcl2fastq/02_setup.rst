.. _bcl2fastq_setup:

Setup
*****

Pre-Setup
=========
SampleSheet.csv
===============
You will need to ensure that a SampleSheet is present at the top level of the folder holding the bcl files
in your project's Google bucket. A working *SampleSheet.csv* is shown below. The demultiplexing should work if you follow this format. Note that the only columns
that are strictly necessary (to our knowledge) are *sample_id*, *sample_name*, and *index*.

.. code-block:: python
    :caption: :file:`SampleSheet.csv`

    [Data],,,,,,,
    Sample_ID,Sample_Name,Sample_Plate,Sample_Well,I7_Index_ID,index,I5_Index_ID,index2
    1,WT1_Th0_1,,A1,N701,TAAGGCGA,S502,ATAGAGAG
    2,WT1_Th0_2,,B1,N701,TAAGGCGA,S503,AGAGGATA
    3,WT2_Th0_1,,C1,N701,TAAGGCGA,S504,TCTACTCT
    4,WT2_Th0_2,,D1,N701,TAAGGCGA,S505,CTCCTTAC
    5,WT3_Th0_1,,E1,N701,TAAGGCGA,S506,TATGCAGT
    6,WT3_Th0_2,,F1,N701,TAAGGCGA,S507,TACTCCTT
    7,KO1_Th0_1,,A2,N702,CGTACTAG,S502,ATAGAGAG
    8,KO1_Th0_2,,B2,N702,CGTACTAG,S503,AGAGGATA
    9,KO2_Th0_1,,C2,N702,CGTACTAG,S504,TCTACTCT
    10,KO2_Th0_2,,D2,N702,CGTACTAG,S505,CTCCTTAC
    11,KO3_Th0_1,,E2,N702,CGTACTAG,S506,TATGCAGT
    12,KO3_Th0_2,,F2,N702,CGTACTAG,S507,TACTCCTT
    13,WT1_Th1_1,,A3,N703,AGGCAGAA,S502,ATAGAGAG
    14,WT1_Th1_2,,B3,N703,AGGCAGAA,S503,AGAGGATA
    15,WT2_Th1_1,,C3,N703,AGGCAGAA,S504,TCTACTCT
    16,WT2_Th1_2,,D3,N703,AGGCAGAA,S505,CTCCTTAC
    17,WT3_Th1_1,,E3,N703,AGGCAGAA,S506,TATGCAGT
    18,WT3_Th1_2,,F3,N703,AGGCAGAA,S507,TACTCCTT
    19,KO1_Th1_1,,A4,N704,TCCTGAGC,S502,ATAGAGAG
    20,KO1_Th1_2,,B4,N704,TCCTGAGC,S503,AGAGGATA
    21,KO2_Th1_1,,C4,N704,TCCTGAGC,S504,TCTACTCT
    22,KO2_Th1_2,,D4,N704,TCCTGAGC,S505,CTCCTTAC
    23,KO3_Th1_1,,E4,N704,TCCTGAGC,S506,TATGCAGT
    24,KO3_Th1_2,,F4,N704,TCCTGAGC,S507,TACTCCTT
    25,WT1_Th17n_1,,A5,N705,GGACTCCT,S502,ATAGAGAG
    26,WT1_Th17n_2,,B5,N705,GGACTCCT,S503,AGAGGATA
    27,WT2_Th17n_1,,C5,N705,GGACTCCT,S504,TCTACTCT
    28,WT2_Th17n_2,,D5,N705,GGACTCCT,S505,CTCCTTAC
    29,WT3_Th17n_1,,E5,N705,GGACTCCT,S506,TATGCAGT
    30,WT3_Th17n_2,,F5,N705,GGACTCCT,S507,TACTCCTT
    31,KO1_Th17n_1,,A6,N706,TAGGCATG,S502,ATAGAGAG
    32,KO1_Th17n_2,,B6,N706,TAGGCATG,S503,AGAGGATA
    33,KO2_Th17n_1,,C6,N706,TAGGCATG,S504,TCTACTCT
    34,KO2_Th17n_2,,D6,N706,TAGGCATG,S505,CTCCTTAC
    35,KO3_Th17n_1,,E6,N706,TAGGCATG,S506,TATGCAGT
    36,KO3_Th17n_2,,F6,N706,TAGGCATG,S507,TACTCCTT
    37,WT1_Th17p_1,,A7,N707,CTCTCTAC,S502,ATAGAGAG
    38,WT1_Th17p_2,,B7,N707,CTCTCTAC,S503,AGAGGATA
    39,WT2_Th17p_1,,C7,N707,CTCTCTAC,S504,TCTACTCT
    40,WT2_Th17p_2,,D7,N707,CTCTCTAC,S505,CTCCTTAC
    41,WT3_Th17p_1,,E7,N707,CTCTCTAC,S506,TATGCAGT
    42,WT3_Th17p_2,,F7,N707,CTCTCTAC,S507,TACTCCTT
    43,KO1_Th17p_1,,A8,N708,CAGAGAGG,S502,ATAGAGAG
    44,KO1_Th17p_2,,B8,N708,CAGAGAGG,S503,AGAGGATA
    45,KO2_Th17p_1,,C8,N708,CAGAGAGG,S504,TCTACTCT
    46,KO2_Th17p_2,,D8,N708,CAGAGAGG,S505,CTCCTTAC
    47,KO3_Th17p_1,,E8,N708,CAGAGAGG,S506,TATGCAGT
    48,KO3_Th17p_2,,F8,N708,CAGAGAGG,S507,TACTCCTT
    49,WT1_Treg_1,,A9,N709,GCTACGCT,S502,ATAGAGAG
    50,WT1_Treg_2,,B9,N709,GCTACGCT,S503,AGAGGATA
    51,WT2_Treg_1,,C9,N709,GCTACGCT,S504,TCTACTCT
    52,WT2_Treg_2,,D9,N709,GCTACGCT,S505,CTCCTTAC
    53,WT3_Treg_1,,E9,N709,GCTACGCT,S506,TATGCAGT
    54,WT3_Treg_2,,F9,N709,GCTACGCT,S507,TACTCCTT
    55,KO1_Treg_1,,A10,N710,CGAGGCTG,S502,ATAGAGAG
    56,KO1_Treg_2,,B10,N710,CGAGGCTG,S503,AGAGGATA
    57,KO2_Treg_1,,C10,N710,CGAGGCTG,S504,TCTACTCT
    58,KO2_Treg_2,,D10,N710,CGAGGCTG,S505,CTCCTTAC
    59,KO3_Treg_1,,E10,N710,CGAGGCTG,S506,TATGCAGT
    60,KO3_Treg_2,,F10,N710,CGAGGCTG,S507,TACTCCTT

Configuration
=============

experiment_name
---------------
Refers to the common Xavier Lab naming convention of <date_initials>. This will help set the Google bucket location of the
input and output files for the experiment; *genomics_xavier_bucket/<experiment_type>/<experiment_name>*. As is customary, the *bcl* files that
serve as the input to the pipeline must be in *genomics_xavier_bucket/<experiment_type>/<experiment_name>/bcl* and the *fastq* files will
be written to *genomics_xavier_bucket/<experiment_type>/<experiment_name>/fastq*.

experiment_type
---------------
Denotes the type of experiment that was run. It must correspond to one of the directories listed `here <https://console.cloud.google.com/storage/browser/genomics_xavier_bucket?project=genomics-xavier>`_,
as this parameter is used to set the location for output.

read_pairs_file
---------------
Location of the `read_pairs.tsv` file that will be generated as part of the pipeline.
