.. _tf_seq_output:

Output
******

The pipeline will produce various UMI counts/stats files. They will be placed in *genomics_xavier_bucket/TFSeq/<experiment_name>/<plate_name>*,
where *experiment_name* and *plate_name* are set by the user in the input configuration.
