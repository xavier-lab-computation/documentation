.. _tf_seq_overview:

Overview
********

FireCloud Method: xavier-genomics/tf_seq

The TF-Seq pipeline accepts fastq files from a TF-Seq experiment and outputs UMI counts and statistics for each plate.
