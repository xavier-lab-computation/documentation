.. _3-5_prime_rna_seq_overview:

Overview
********
The 3'/5' RNA-Seq pipeline is currently set up to run only on the Broad server, so there is no FireCloud method.
There are a few different versions floating around, but the definitive version is on
`GitLab <https://gitlab.com/xavier-lab-computation/pipelines/host/3-5_prime_rna_seq>`_.

Documentation for this pipeline will stay on the GitLab for now (see the README associated with the repository), until someone
decides to move this pipeline to the cloud.

Note that this pipeline also (theoretically) has the capability to process paired-end RNA-Seq data, but
we have had trouble getting it to work.
