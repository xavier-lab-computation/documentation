.. _terra_setup:

Terra Setup
************

Terra Groups
-------------
Individual lab members should not have to worry too much about this, but it is
important for administrators.
Terra workspaces and billing can be associated with a Terra group, which gives us a
way to streamline the process of adding new members (see below).

If you want to establish a new group, we believe the easiest way is to talk to BITS about associating a
Terra group with a Google Cloud Cost Object. Once that has been done, all work done in the group's workspace
will be paid for with that cost object.

New Users
---------
Giving new lab members access to the Terra workspace should be as easy as adding them to the Terra group. In order for them to use particular methods however, either
the method must be publicly readable or they must be made an owner. As of March 2019, you cannot give group-level permissions to a method; so individuals must be granted ownership
individually.

New Google Bucket Access
------------------------
In order for a Terra pipeline to access Google Buckets, the Terra workspace user group must be added as *Storage Admin* to the bucket in question.
This can be done by navigating to the info panel for a given bucket (either by clicking the *Show Info Panel* link or by selecting the dots to the right of each
bucket) and creating a new member at the *Storage Admin* level. We recommend looking at the permissions for the *genomics_xavier_bucket* bucket for how best to
achieve this.

.. note::
    As of May 2019, you can still give access to a Terra workspace user group by using the email *<x>-users@firecloud.org*, where *<x>* is the name of the Terra workspace
    whose members you would like to have access to the Google Bucket. This may eventually change (e.g. to *<x>-users@terra.org*) when the transition from FireCloud to Terra is complete.
