.. _command_line:

Command Line
************

The command line submission tool was provided to us by Josh Gould from the Regev Lab, so we are linking to the documentation he wrote
`here <https://kco-cloud.readthedocs.io/en/latest/command_line.html>`_.

For our purposes, we have not found the need to request an interactive session. This may be necessary if you are on a login node, but should not be
if you are on a node with adequate resources (e.g. *boltzmann*).
