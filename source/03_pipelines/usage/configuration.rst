.. _configuration:

Configuration
*************
Each pipeline has sample configuration files included in the associated GitLab repository. The variable definitions should be included in the **Setup**
portion of the documentation, but we still recommend using the sample configuration files as a template for creating your own.
If the pipeline has been updated at any point, it is possible that some of the old example configuration files will be out of date. However, the configuration file
named *sample_config.json* should always be up to date with the most recent version of the pipeline.  
