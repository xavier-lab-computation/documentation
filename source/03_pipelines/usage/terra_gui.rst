.. _terra_gui:

Terra GUI
*********

Job Submission
==============
If you do not want to use the command line tool (see :ref:`command_line`), you can submit pipeline runs with the Terra GUI. The steps to do that
are as follows:

1. Navigate to your workspace
2. Click on the *Tools* tab, then select the method corresponding to your pipeline (you may need to add the method by clicking *Find a Tool*)
3. Ensure (unless you know you have reason to do otherwise) that you are using the most recent snapshot (the largest number) and select the option *Process single workflow from files*
4. Select the *Inputs* button and click on the *upload json* link
5. Upload your pipeline configuration file
6. Click the *Run Analysis* button

Job Status
==========
You can check the status of your job(s) in the *Job History* tab. Each submission has an associated submission ID
and will provide information regarding its status, run cost, etc.
