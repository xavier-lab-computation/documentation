.. _bulk_rna_seq_overview:

Overview
********

FireCloud Method: xavier-genomics/bulk_rna_seq

The RNA-Seq pipelines takes raw reads (in *fastq* format) and outputs QC metrics, a gene counts matrix, and differential expression between specified conditions.
Currently, it accepts data only from paired-end experiments, but we are working to include 3'/5' DGE as well. The steps (broadly) are as follows:

1. Create a kallisto index for the specified reference transcriptome (if this has not already been done)

2. Perform pre-alignment QC on each sample

3. Pseudoalign reads to reference transcriptome, generate transcript counts, and get alignment QC metrics (via *kallisto*)

4. Map from transcript counts to gene counts, output gene counts (if user wants to run differential expression via *edgeR* on their own),
and perform differential expression (via *sleuth*)

5. Combine pre-alignment and alignment QC via *multiQC*
