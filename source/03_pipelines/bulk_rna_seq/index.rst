.. _bulk_rna_seq:

Bulk RNA-Seq Pipeline
**********************

.. toctree::
    :maxdepth: 1
    :titlesonly:

    01_overview
    02_setup
    03_output
    04_troubleshooting
