.. _bulk_rna_seq_troubleshooting:

Troubleshooting
***************

Core Dump
---------
If you get a *Aborted (core dumped)* error in the *perform_kallisto_quantification* step, it is likely that kallisto was not able to align any reads. This could be because of a problem with your *.fastq* files (e.g.
they did not demultiplex correctly), you could be using the wrong reference transcriptome,
or you might be using a reference transcriptome index length (given by the **cycle_number**
parameter in the :ref:`bulk_rna_seq_setup` section) that is larger than your read length.
