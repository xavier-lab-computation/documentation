.. _stars_crispr_screen:

STARS CRISPR Screen Pipeline
****************************

.. toctree::
    :maxdepth: 1
    :titlesonly:

    01_overview
    02_setup
    03_output
    04_troubleshooting
