.. _stars_crispr_screen_output:

Output
******

analysis
========
The *analysis_<experiment_name>.html* file is an interactive html document for viewing the data and visualizations. This is a
work-in-progress, so please feel free to add new capabilities to the document or open a gitlab issue and submit an Enhancement request.

stars_output
============
Contains the *full_stars_output.tsv*, which is the master output file containing the base results from the pipeline. An example is show in the :ref:`stars_crispr_screen_overview`.

.. NOTE::
    To stay consistent with the lab's existing base differential expression code, the log fold change is defined such that a negative log fold change for a comparison **x_vs_y**
    means higher counts in **y** than in **x**. 
