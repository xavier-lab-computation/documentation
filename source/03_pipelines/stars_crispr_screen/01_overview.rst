.. _stars_crispr_screen_overview:

Overview
********

FireCloud Method: xavier-genomics/stars_crispr_screen

The STARS CRISPR Screen pipeline takes raw pooled reads (in *.fastq* format) and does the following:

1. De-pools samples and generates counts for various genes
2. If there are multiple complete runs of the sequencing, combine them and treat the separate runs as replicates.
3. Run edgeR to get differential expression for pairs of conditions.
4. Combine differential expression results across pairs of conditions.
5. Perform STARS analysis to identify top gene/comparison combinations.

The eventual output is a file called *full_stars_output.tsv* that will look something like the following (with many more genes):

.. csv-table:: full_stars_output.tsv
    :file: ./full_stars_output_example.csv

There are a number of ways to do CRISPR Screen analyses and we may create more pipelines in the future.
