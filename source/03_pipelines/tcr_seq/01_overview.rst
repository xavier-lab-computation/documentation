.. _tcr_seq_overview:

Overview
********
The TCR-Seq pipeline is currently set up to run only on the Broad server, so there is no FireCloud method.
Moran Yassour wrote most of the core code and Jason Bishai wrote some of the higher-level scripts.
