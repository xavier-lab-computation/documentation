.. _simple_variant_calling_overview:

Overview
********

FireCloud Method: xavier-genomics/simple_variant_calling

This is a very simple variant calling pipeline modeled after
`this <https://www.ebi.ac.uk/sites/ebi.ac.uk/files/content.ebi.ac.uk/materials/2014/140217_AgriOmics/dan_bolser_snp_calling.pdf>`_
from the European Bioinformatics Institute. It accepts a reference genome and information about your fastq files
and returns a set of VCF files with variants present in your samples. There will be one VCF file for each *group* as defined by the user.
The original use case was to have each group be a donor.
