.. _simple_variant_calling_troubleshooting:

Troubleshooting
***************

Multiple ALTs
-------------
There is an unaccounted for edge case in which a single variant has multiple ALT values.
In this case, the counts for numbers of hom_ref, het, etc. will be wrong (or the script might just fail).
Christian could not find a variant where this was the case, and thus was not sure exactly how the underlying
data (in the VCF file) are represented in this case. If someone can find a variant with multiple ALTS,
it should be relatively easy to update the *aggregate_vcf.py* script to handle this.
