.. _simple_variant_calling:

Simple Variant Calling Pipeline
*******************************

.. toctree::
    :maxdepth: 1
    :titlesonly:

    01_overview
    02_setup
    03_output
    04_troubleshooting
