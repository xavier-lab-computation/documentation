.. _simple_variant_calling_output:

Output
******
The pipeline will create a directory called *vcf_directory* with subdirectories containing vcf files
for each requested region, as well as an aggregate dataset called *identified_variants.tsv*.
