.. _utilities:

Utilities
*********
The `utilities <https://gitlab.com/xavier-lab-computation/utilities>`_ subgroup is a grab-bag of useful items that don't fit cleanly into
the definition of a pipeline. The structure is still a work-in-progress and will likely remain fluid, so documentation of this section
may take place in the README files associated with each project or as comments in the scripts themselves.
