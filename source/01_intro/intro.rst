.. _introduction:

Introduction
************
The `Xavier Lab Computation Group <https://gitlab.com/xavier-lab-computation>`_ is a central hub for all things
related to computational tools commonly used in the Xavier Lab.

.. Each section of the lab (*host* and *microbiome*) has their own
.. subdirectory within the repository which will contain pipelines relevant to their common tasks. Each pipeline will have its own
.. associated directory which will contain scripts that comprise the pipeline, documentation explaining necessary pre-processing steps
.. and input files, and comments on problems a user might face when trying to run the pipeline.
..
.. Once the user understands the necessary pre-processing and input files, the pipelines should be easy to run and rely on a shared
.. submission script. The primary idea behind all of this (the central repository, shared submission method, and consistent documentation method)
.. is to streamline the learning process when a user is tasked with running a pipeline. Ideally, users will contribute new features
.. (when necessary) to the pipelines, add documentation, and note new problems/solutions as they encounter them.
