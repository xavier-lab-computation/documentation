Software
********
The pipeline uses the following free and open-source software. We thank the developers for their great work on all of these projects.

* General Programming: `python <https://www.python.org/>`_, `R <https://www.r-project.org/>`_
* Alignment: `HISAT2 <http://ccb.jhu.edu/software/hisat2/index.shtml>`_, `Bowtie 2 <http://bowtie-bio.sourceforge.net/bowtie2/index.shtml>`_, `BWA <http://bio-bwa.sourceforge.net/>`_
* Counting: `htseq <https://htseq.readthedocs.io/en/release_0.10.0/>`_
* Differential Gene Expression analysis: `edgeR <https://bioconductor.org/packages/release/bioc/html/edgeR.html>`_
* QC Analysis: `RSeqC <http://rseqc.sourceforge.net/>`_, `FastQC <https://www.bioinformatics.babraham.ac.uk/projects/fastqc/>`_
* Graphics: `ggplot2 <https://ggplot2.tidyverse.org/>`_, `pheatmap <https://cran.r-project.org/web/packages/pheatmap/pheatmap.pdf>`_
* Documentation: `Sphinx <http://www.sphinx-doc.org/en/master/>`_
