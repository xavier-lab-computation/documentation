Configuring the Pipeline
************************

The pipeline accepts a number of parameters:

 * experiment_name: The name assigned to the experiment that generated the data we are running through the pipeline.
 For the Xavier Lab, this is something like 2018_08_24_CC.
 * organism: The name of the organism from which the samples came. Supported arguments are *human* and *mouse*.
 * queue: <I don't actually understand this argument yet -- it has something to do with read length?>. Supported arguments are *short* and *long*.
 * start_index: Index of first step in the pipeline you wish to run
 * end_index: Index of last step in the pipeline you wish to run
 * functional_analysis:
 * read_pairs_file:
 * barcodes_file:
 * samples_described_file:
 * samples_compared_file:
 * reads_dir:
 * alignment_dir:
