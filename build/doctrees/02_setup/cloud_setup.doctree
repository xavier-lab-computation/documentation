���      �docutils.nodes��document���)��}�(�	rawsource�� ��children�]�h �section���)��}�(hhh]�(h �title���)��}�(h�Setting up Google Cloud�h]�h �Text����Setting up Google Cloud�����}�(hh�parent�hhh�source�N�line�Nuba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�hhhhhh�_/home/BROAD.MIT.EDU/ccovingt/broad/rnaSeqPipeline/documentation/source/02_setup/cloud_setup.rst�hKubh �	paragraph���)��}�(hX�  The pipeline utilizes two different products within the Google Cloud ecosystem; `Compute Engine <https://cloud.google.com/compute/>`_ and
`Cloud Storage <https://cloud.google.com/storage/>`_. As the names imply, the pipeline performs computations using the compute engine and stores data in cloud storage.
If you are at the Broad Institute, you should be able to follow `this link <https://console.cloud.google.com/>`_ and use your Broad gmail account to sign into the platform. The
Xavier Lab has a project called **genomics-xavier**, and this is the project under which all activities take place. This is also the level of organization at which payment takes place.�h]�(h�PThe pipeline utilizes two different products within the Google Cloud ecosystem; �����}�(h�PThe pipeline utilizes two different products within the Google Cloud ecosystem; �hh-hhhNhNubh �	reference���)��}�(h�5`Compute Engine <https://cloud.google.com/compute/>`_�h]�h�Compute Engine�����}�(hhhh8ubah}�(h]�h!]�h#]�h%]�h']��name��Compute Engine��refuri��!https://cloud.google.com/compute/�uh)h6hh-ubh �target���)��}�(h�$ <https://cloud.google.com/compute/>�h]�h}�(h]��compute-engine�ah!]�h#]��compute engine�ah%]�h']��refuri�hIuh)hJ�
referenced�Khh-ubh� and
�����}�(h� and
�hh-hhhNhNubh7)��}�(h�4`Cloud Storage <https://cloud.google.com/storage/>`_�h]�h�Cloud Storage�����}�(hhhh_ubah}�(h]�h!]�h#]�h%]�h']��name��Cloud Storage�hH�!https://cloud.google.com/storage/�uh)h6hh-ubhK)��}�(h�$ <https://cloud.google.com/storage/>�h]�h}�(h]��cloud-storage�ah!]�h#]��cloud storage�ah%]�h']��refuri�houh)hJhYKhh-ubh��. As the names imply, the pipeline performs computations using the compute engine and stores data in cloud storage.
If you are at the Broad Institute, you should be able to follow �����}�(h��. As the names imply, the pipeline performs computations using the compute engine and stores data in cloud storage.
If you are at the Broad Institute, you should be able to follow �hh-hhhNhNubh7)��}�(h�0`this link <https://console.cloud.google.com/>`_�h]�h�	this link�����}�(hhhh�ubah}�(h]�h!]�h#]�h%]�h']��name��	this link�hH�!https://console.cloud.google.com/�uh)h6hh-ubhK)��}�(h�$ <https://console.cloud.google.com/>�h]�h}�(h]��	this-link�ah!]�h#]��	this link�ah%]�h']��refuri�h�uh)hJhYKhh-ubh�a and use your Broad gmail account to sign into the platform. The
Xavier Lab has a project called �����}�(h�a and use your Broad gmail account to sign into the platform. The
Xavier Lab has a project called �hh-hhhNhNubh �strong���)��}�(h�**genomics-xavier**�h]�h�genomics-xavier�����}�(hhhh�ubah}�(h]�h!]�h#]�h%]�h']�uh)h�hh-ubh��, and this is the project under which all activities take place. This is also the level of organization at which payment takes place.�����}�(h��, and this is the project under which all activities take place. This is also the level of organization at which payment takes place.�hh-hhhNhNubeh}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKhhhhubh,)��}�(h�yThe first step is to move your data from the Broad cluster to cloud storage. You can do that through the following steps:�h]�h�yThe first step is to move your data from the Broad cluster to cloud storage. You can do that through the following steps:�����}�(hh�hh�hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK	hhhhubh �enumerated_list���)��}�(hhh]�(h �	list_item���)��}�(h�Log onto the Broad cluster
�h]�h,)��}�(h�Log onto the Broad cluster�h]�h�Log onto the Broad cluster�����}�(hh�hh�ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKhh�ubah}�(h]�h!]�h#]�h%]�h']�uh)h�hh�hhhh*hNubh�)��}�(hX2  Carry out the following shell commands::

 # load google cloud software development kit
 use .google-cloud-sdk

 # authenticate (this should be necessary only for the first time using the google cloud sdk)
 gcloud auth login

 # identify the google cloud project
 gcloud config set project genomics-xavier
�h]�(h,)��}�(h�(Carry out the following shell commands::�h]�h�'Carry out the following shell commands:�����}�(h�'Carry out the following shell commands:�hh�ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKhh�ubh �literal_block���)��}�(hX  # load google cloud software development kit
use .google-cloud-sdk

# authenticate (this should be necessary only for the first time using the google cloud sdk)
gcloud auth login

# identify the google cloud project
gcloud config set project genomics-xavier�h]�hX  # load google cloud software development kit
use .google-cloud-sdk

# authenticate (this should be necessary only for the first time using the google cloud sdk)
gcloud auth login

# identify the google cloud project
gcloud config set project genomics-xavier�����}�(hhhj  ubah}�(h]�h!]�h#]�h%]�h']��	xml:space��preserve�uh)j   hKhh�ubeh}�(h]�h!]�h#]�h%]�h']�uh)h�hh�hhhh*hNubeh}�(h]�h!]�h#]�h%]�h']��enumtype��arabic��prefix�h�suffix��.�uh)h�hhhhhh*hKubh,)��}�(h�QCongratulations! At this point, you should be able to interact with Google Cloud!�h]�h�QCongratulations! At this point, you should be able to interact with Google Cloud!�����}�(hj%  hj#  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKhhhhubeh}�(h]��setting-up-google-cloud�ah!]�h#]��setting up google cloud�ah%]�h']�uh)h	hhhhhh*hKubah}�(h]�h!]�h#]�h%]�h']��source�h*uh)h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(hN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j\  �error_encoding��UTF-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h*�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�N�gettext_compact��ub�reporter�N�indirect_targets�]��substitution_defs�}��substitution_names�}��refnames�}��refids�}��nameids�}�(j6  j3  hUhRhyhvh�h�u�	nametypes�}�(j6  NhU�hy�h��uh}�(j3  hhRhLhvhph�h�u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �id_start�K�parse_messages�]��transform_messages�]��transformer�N�
decoration�Nhhub.